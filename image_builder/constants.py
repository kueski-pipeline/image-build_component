IMAGE_NAME = "gcr.io/kaniko-project/executor:latest"
IMAGE_TAG = "latest"


DISPLAY_NAME = "Build image on k8s"

MEMORY_REQUEST = "1Gi"
MEMORY_LIMIT = "100Mi"

CPU_REQUEST = "200m"
CPU_LIMIT = "1000m"

LOGLEVEL = "debug"
S3_ENDPOINT = "http://minio.10.0.0.200.nip.io:80"
AWS_ACCESS_KEY_ID = "minio"
AWS_SECRET_ACCESS_KEY = "minio123"
DOCKER_HUB_SECRET = ""


DEFAULT_SPARK_MASTER = "local[*]"