from kfp import dsl
from kfp.dsl import ContainerOp

from image_builder.configure import setup_resources, add_environments
from image_builder.constants import IMAGE_NAME,\
    IMAGE_TAG, CPU_LIMIT, CPU_REQUEST, MEMORY_LIMIT, DISPLAY_NAME, MEMORY_REQUEST


@dsl.component
def make_image_builder(
        context_path: str = "", model_path: str = "", resulting_image_name: str = "",
        resulting_image_tag: str = "",
        
        image_name: str = IMAGE_NAME, image_tag: str = IMAGE_TAG,
        display_name: str = DISPLAY_NAME, memory_request: int = MEMORY_REQUEST,
        memory_limit: int = MEMORY_LIMIT, cpu_request: int = CPU_REQUEST, cpu_limit: int = CPU_LIMIT):

    image = f"{image_name}:{image_tag}"
    operator = ContainerOp(
        image=image,
        name="main",
        arguments=[
            f"--context={context_path}",
            f"--destination={resulting_image_name}:{resulting_image_tag}",
            f"--build-arg \"MODEL_PATH='{model_path}'\""
        ]
    )

    operator = operator.set_display_name(display_name)
    operator = setup_resources(operator, memory_request, memory_limit, cpu_request, cpu_limit)
    operator = add_environments(operator)
    operator.execution_options.caching_strategy.max_cache_staleness = "P0D"

    return operator
